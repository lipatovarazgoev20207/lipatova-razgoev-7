#include <iostream>
#include <ctime>
#include <iomanip>

static int** matrix;
using namespace std;
static int SizeOfMatrix;
int** CreateMatrix()
{
	int RangeOfRandom;
	cout << "Input size of matrix: ";
	cin >> SizeOfMatrix;
	cout << "Input range of numbers: ";
	cin >> RangeOfRandom;
	srand(time(0));
	matrix = new int* [SizeOfMatrix];
	for (int i = 0; i < SizeOfMatrix; i++)
	{
		matrix[i] = new int[SizeOfMatrix];
	}
	for (int i = 0; i < SizeOfMatrix; i++)
	{
		for (int j = 0; j < SizeOfMatrix; j++)
		{
			matrix[i][j] = -RangeOfRandom + rand() % (2 * RangeOfRandom + 1);
		}
	}
	return matrix;
}
int** ShowMatrix()
{
	for (int i = 0; i < SizeOfMatrix; i++)
	{
		for (int j = 0; j < SizeOfMatrix; j++)
		{
			printf("%4d", matrix[i][j]);
		}
		cout << "\n";
	}
	return matrix;
}
int SearchBellowElement()
{
	int CounterFinds, GivenValue;
	CounterFinds = 0;
	cout << "Input element to find quanity of bellow: ";
	cin >> GivenValue;
	for (int i = 0; i < SizeOfMatrix; i++)
	{
		for (int j = 0; j < SizeOfMatrix; j++)
		{
			if ((matrix[i][j]) < GivenValue)
			{
				CounterFinds++;
			}
		}
	}
	cout << "Quanity of elements: " << CounterFinds << endl;
	return CounterFinds;
}
int NonzeroColomns()
{
	int NonzeroColomnSum = 0;
	bool NoneZeroElement;
	for (int i = 0; i < SizeOfMatrix; i++)
	{
		NoneZeroElement = false;
		for (int j = 0; j < SizeOfMatrix; j++)
		{
			if (matrix[i][j] != 0)
			{
				NoneZeroElement = true;
				break;
			}
		}
		if (NoneZeroElement == true)
		{
			NonzeroColomnSum++;
		}
	}
	cout << "Quanity of nonzero coloms in matrix: " << NonzeroColomnSum << endl;
	return NonzeroColomnSum;
}
int SumUnderMainDiagonal()
{
	int Sum = 0;
	for (int i = 0; i < SizeOfMatrix; i++)
	{
		for (int j = 0; j < SizeOfMatrix; j++)
		{
			if (j <= i)
			{
				Sum+=matrix[i][j];
			}
		}
	}
	cout << "Sum of elements on main diagnal: " << Sum << endl;
	return Sum;
}